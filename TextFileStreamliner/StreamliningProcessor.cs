﻿using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using iText.Kernel.Pdf.Canvas.Parser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Text.RegularExpressions;
using iText.Layout;
using Microsoft.Office.Interop.Word;
using System.DirectoryServices.ActiveDirectory;

namespace TextFileStreamliner
{
    public partial class StreamliningProcessor
    {
        List<FileInfo> filesToProcess = new List<FileInfo>();
        List<FileInfo> processedFiles = new List<FileInfo>();
        List<FileInfo> unprocessableFiles = new List<FileInfo>();
        List<FileInfo> duplicateFiles = new List<FileInfo>();
        static List<string> knownFileNames = new List<string>();
        BackgroundWorker? worker;

        public int TotalFileCount => filesToProcess.Count;
        public int ProcessedFileCount => processedFiles.Count;
        public int DuplicateFileCount => duplicateFiles.Count;
        public int UnprocessableFileCount => unprocessableFiles.Count;
        public float Progress => TotalFileCount != 0 ? Math.Clamp((float)ProcessedFileCount / TotalFileCount, 0, 1) : 1;
        public string? CurrentFile { get; private set; }

        private void Initialize()
        {
            DirectoryInfo directory = new DirectoryInfo(Properties.Settings.Default.currentInflowFolder);
            filesToProcess = directory.GetFiles().ToList();
            processedFiles.Clear();
            unprocessableFiles.Clear();
            duplicateFiles.Clear();

        }

        public void StartProcessing(BackgroundWorker worker)
        {
            Initialize();
            this.worker = worker;
            foreach (FileInfo file in filesToProcess)
            {
                if (worker.CancellationPending)
                {
                    return;
                }
                try
                {
                    ProcessFile(file);
                }
                catch(Exception ex)
                {
                    MessageBox.Show($"Ran into an exception trying to process file {file.Name}:\n\n{ex.Message}", "Exception during processing");
                    unprocessableFiles.Add(file);
                }
        }
            ReportProgress();
        }

        private void ReportProgress()
        {
            worker?.ReportProgress((int)(Progress * 100));
        }

        private void ProcessFile(FileInfo file)
        {
            if (Properties.Settings.Default.shouldRemoveDuplicates && DuplicateManager.IsDuplicate(file))
            {
                duplicateFiles.Add(file);
                MoveFileToFolder(file, Properties.Settings.Default.currentDuplicateFolder);
            }
            else
            {
                ExtractAndMoveContentToRtf(file);
            }
            if (Properties.Settings.Default.shouldKeepOriginalFile && !unprocessableFiles.Contains(file))
            {
                MoveFileToFolder(file, Properties.Settings.Default.currentOriginalsFolder);
            }
            processedFiles.Add(file);
            ReportProgress();
        }

        private void ExtractAndMoveContentToRtf(FileInfo file)
        {
            CurrentFile = file.Name;
            if(TryGetContent(file, out string fileContent))
            {
                fileContent = ApplyTextTransforms(fileContent);
                WriteToRtfFile(file, fileContent);
            }
            else
            {
                unprocessableFiles.Add(file);
            }
        }

        #region content retrieval
        private static bool TryGetContent(FileInfo file, out string content)
        {
            content = "";
            switch(file.Extension)
            {
                case ".txt":
                    content = GetContentFromTxt(file);
                    break;
                case ".rtf":
                    content = GetContentFromRtf(file);
                    break;
                case ".pdf":
                    content = GetContentFromPdf(file);
                    break;
                case ".doc":
                case ".docx":
                case ".odt":
                    content = GetContentFromWordFile(file);
                    break;
                default:
                    Debug.WriteLine($"unprocessable file extension: {file.Extension}");
                    break;
            }
            return !string.IsNullOrEmpty(content);
        }

        private static string GetContentFromTxt(FileInfo file)
        {
            Encoding encoding;
            using (FileStream fs = File.OpenRead(file.FullName))
            {
                encoding = EncodingDetector.GetEncoding(fs);
            }
            //Debug.WriteLine($"File {file.FullName} has encoding {encoding}");
            return File.ReadAllText(file.FullName, encoding);
        }

        private static string GetContentFromPdf(FileInfo file)
        {
            string content = "";
            using (PdfReader pdfReader = new PdfReader(file.FullName))
            using(PdfDocument pdfDocument = new PdfDocument(pdfReader))
            {
                for (int page = 1; page <= pdfDocument.GetNumberOfPages(); page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    content += PdfTextExtractor.GetTextFromPage(pdfDocument.GetPage(page), strategy);
                }
            }
            return content;
        }

        private static string GetContentFromWordFile(FileInfo file)
        {
            Microsoft.Office.Interop.Word.Application? application = null;
            Microsoft.Office.Interop.Word.Document? document = null;
            string content = "";
            try
            {
                application = new();
                document = application.Documents.Open(file.FullName);
                content = document.Content.Text;
            }
            finally
            {
                document?.Close();
                application?.Quit();
            }
            return content;
        }

        private static string GetContentFromRtf(FileInfo file)
        {
            using(RichTextBox rtf = new RichTextBox())
            {
                rtf.Rtf = File.ReadAllText(file.FullName);
                return rtf.Text;
            }
        }
        #endregion

        #region file movement
        private static void MoveFileToFolder(FileInfo file, string folder)
        {
#if DEBUG
            Debug.WriteLine($"NOT MOVING FILE DUE TO DEBUG MODE: {file.FullName}");
            return;
#endif
            string newFilePath = folder + "\\" + file.Name;
            if (!File.Exists(newFilePath))
            {
                string oldFilePath = file.FullName;
                file.MoveTo(newFilePath);
                Debug.WriteLine($"Moved original file {oldFilePath} to {newFilePath}");
            }
            else
            {
                File.Delete(file.FullName);
                Debug.WriteLine($"Deleted original file {file.FullName}");
            }
        }

        static readonly RichTextBox rtfOutputBox = new();
        private static void WriteToRtfFile(FileInfo file, string fileContent)
        {
            rtfOutputBox.Text = fileContent;
            rtfOutputBox.SelectAll();
            rtfOutputBox.SelectionFont = new System.Drawing.Font("Arial", 14);
            string outputPath = GetOutputPathFor(file);
            rtfOutputBox.SaveFile(outputPath);
            Debug.WriteLine($"Processed and wrote file {outputPath}");
        }

        private static string GetOutputPathFor(FileInfo file)
        {
            return Properties.Settings.Default.currentOutputFolder + "\\" + file.Name + ".rtf";
        }
        #endregion

        #region text transforms
        private static string ApplyTextTransforms(string text)
        {
            text = ApplyUniversalTextTransforms(text);
            if (Properties.Settings.Default.shouldTransformMaleToFemale)
            {
                text = ApplyMaleToFemaleTransforms(text);
            }
            if (Properties.Settings.Default.shouldTransformIntersexToFemale)
            {
                text = ApplyIntersexToFemaleTransforms(text);
            }
            return text;
        }

        [GeneratedRegex("…")]
        private static partial Regex TripleDotCharToTripleDots();

        [GeneratedRegex(" +")]
        private static partial Regex RepeatedWhitespace();

        [GeneratedRegex("(?<!(?:\\.|\\\"|“|”))(\\s+)$\\n", RegexOptions.Multiline)]
        private static partial Regex LineBreaksWithinSentence();

        [GeneratedRegex("\\n\\n+")]
        private static partial Regex RepeatedLinebreaks();
        private static string ApplyUniversalTextTransforms(string text)
        {
            text = TripleDotCharToTripleDots().Replace(text, "...");
            text = RepeatedWhitespace().Replace(text, " ");
            text = LineBreaksWithinSentence().Replace(text, "$1");
            text = RepeatedLinebreaks().Replace(text, "\n\n");
            return text.Trim();
        }

        private static string ApplyMaleToFemaleTransforms(string text)
        {
            List<(string search, string replacement)> replacementStrings = new()
            {
                ("him", "her"),
                ("his", "her"),
                ("he", "she"),
                ("himself", "herself"),
                ("hisself", "herself"),
                ("male", "female"),
                ("man", "woman"),
                ("men", "women"),
                ("guy", "girl"),
                ("boy", "girl"),
                ("dude", "girl"),
                ("lad", "lass"),
                ("bro", "sis"),
                ("brother", "sister"),
                ("father", "mother"),
                ("dad", "mom"),
                ("daddy", "mommy"),
                ("king", "queen"),
            };

            text = ApplyReplacementPatterns(text, replacementStrings);
            return text;
        }

        private static string ApplyIntersexToFemaleTransforms(string text)
        {
            List<(string search, string replacement)> replacementStrings = new()
            {
                ("hir", "her"),
                ("hyr", "her"),
                ("shi", "she"),
            };

            text = ApplyReplacementPatterns(text, replacementStrings);
            return text;
        }

        private static List<(string search, string replace)> ToReplacementPattern(string word, string replacement, bool addUppercase = true, bool addPlural = true)
        {
            const string prefixBoundaryPattern = @"(?<=(^|\s|\b))";
            const string postfixBoundaryPattern = @"(?=($|\s|\b))";

            List<(string, string)> patterns = new List<(string, string)>
            {
                ($"{prefixBoundaryPattern}{word}{postfixBoundaryPattern}", replacement)
            };

            if (addPlural)
                patterns.Add(($"{prefixBoundaryPattern}{word}s{postfixBoundaryPattern}", $"{replacement}s"));
            if (addUppercase)
                patterns.Add(($"{prefixBoundaryPattern}{CapitalizeFirstCharacter(word)}{postfixBoundaryPattern}", CapitalizeFirstCharacter(replacement)));
            if (addUppercase && addPlural)
                patterns.Add(($"{prefixBoundaryPattern}{CapitalizeFirstCharacter(word)}s{postfixBoundaryPattern}", CapitalizeFirstCharacter($"{replacement}s")));

            return patterns;
        }

        private static string ApplyReplacementPatterns(string text, List<(string search, string replacement)> replacementStrings)
        {
            List<(string search, string replacement)> replacementPatterns = new();

            foreach ((string search, string replacement) in replacementStrings)
            {
                replacementPatterns.AddRange(ToReplacementPattern(search, replacement));
            }

            foreach ((string search, string replacement) in replacementPatterns)
            {
                //Debug.WriteLine($"Replacing {search} with {replacement}");
                text = Regex.Replace(text, search, replacement);
            }
            return text;
        }

        private static string CapitalizeFirstCharacter(string word)
        {
            return word[0].ToString().ToUpper() + word.Substring(1);
        }
        #endregion
    }
}
