﻿using Microsoft.VisualBasic.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TextFileStreamliner
{
    public static partial class DuplicateManager
    {
        static HashSet<string> knownFileNames = new HashSet<string>();
        static bool isInitialized;

        public static bool IsDuplicate(FileInfo file)
        {
            if (!isInitialized)
            {
                Initialize();
                //Debug.WriteLine($"files for duplicate-checks: {string.Join(", ", knownFileNames)}");
            }

            string duplicateId = ConvertFileNameToDuplicateIdentifier(file.FullName);

            bool isDuplicate = knownFileNames.Contains(duplicateId);
            //Debug.WriteLine($"{file.FullName} is a duplicate as id {duplicateId} ? {isDuplicate}");
            return isDuplicate;
        }

        public static void Recache()
        {
            knownFileNames.Clear();
            isInitialized = false;
        }

        private static void Initialize()
        {
            if (isInitialized)
            {
                return;
            }

            string rootFolderPath = Properties.Settings.Default.currentOutputFolder;
#if !DEBUG
            knownFileNames = Directory.GetFiles(rootFolderPath, "*.*", SearchOption.AllDirectories)
                .Select(ConvertFileNameToDuplicateIdentifier)
                .ToHashSet();
#endif

            if (Properties.Settings.Default.shouldKeepOriginalFile)
            {
                string originalsFolderPath = Properties.Settings.Default.currentOriginalsFolder;
                IEnumerable<string> originalFiles = Directory.GetFiles(originalsFolderPath, "*.*", SearchOption.AllDirectories)
                    .Select(ConvertFileNameToDuplicateIdentifier);
                foreach (string originalFileDuplicateId in originalFiles)
                {
                    if (!knownFileNames.Contains(originalFileDuplicateId))
                    {
                        knownFileNames.Add(originalFileDuplicateId);
                    }
                }
            }

            isInitialized = true;
        }

        [GeneratedRegex("\\.\\w{2,4}(?=(\\.|$))")]
        private static partial Regex FileExtensionRemoval();
        [GeneratedRegex("^(?:0\\s)?\\d+")]
        private static partial Regex FANumericPrefixRemoval();
        [GeneratedRegex("(\\.|\\s|,|_|-)")]
        private static partial Regex IrrelevantCharacterRemoval();

        private static string ConvertFileNameToDuplicateIdentifier(string name)
        {
            name = name.Substring(name.LastIndexOf('\\') + 1);
            name = FileExtensionRemoval().Replace(name, "");
            name = FANumericPrefixRemoval().Replace(name, "");
            name = IrrelevantCharacterRemoval().Replace(name, "");
            return name;
        }
    }
}
