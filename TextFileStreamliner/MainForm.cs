using Microsoft.Win32;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Transactions;
using TextFileStreamliner.Properties;

namespace TextFileStreamliner
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        Stopwatch elapsedProcessingTimeStopwatch = new Stopwatch();
        static bool hasStartedProcessingOnce = false;
        StreamliningProcessor processor = new StreamliningProcessor();

        string FileCountString => processor != null ? $"File: {processor.ProcessedFileCount}/{processor.TotalFileCount}" : "File: 0/0";
        string DuplicateCountString => processor != null ? $"Duplicates: {processor.DuplicateFileCount}" : "Duplicates: 0";
        string UnprocessableCountString => processor != null && processor.UnprocessableFileCount > 0 ? $"Unprocessable: {processor.UnprocessableFileCount}" : "";

        private void MainWindow_Load(object sender, EventArgs e)
        {
            LoadInitialValuesFromSettings();

            // adds more encodings to .NET Code projects from NuGet package
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        private void LoadInitialValuesFromSettings()
        {
            inflowTxtbox.Text = Properties.Settings.Default.currentInflowFolder;
            outputFolderTxtbox.Text = Properties.Settings.Default.currentOutputFolder;
            duplicatesFolderTxtbox.Text = Properties.Settings.Default.currentDuplicateFolder;
            originalsFolderTxtbox.Text = Properties.Settings.Default.currentOriginalsFolder;
            maleToFemaleTranformCheckbox.Checked = Properties.Settings.Default.shouldTransformMaleToFemale;
            intersexToFemaleTransformCheckbox.Checked = Properties.Settings.Default.shouldTransformIntersexToFemale;
            removeDuplicatesCheckbox.Checked = Properties.Settings.Default.shouldRemoveDuplicates;
            keepOriginalsCheckbox.Checked = Properties.Settings.Default.shouldKeepOriginalFile;

            SetProcessingControlsVisibility(false);
            SetDuplicateControlsVisibility();
            SetOutputsControlsVisibility();
        }

        #region folder pickers
        private void inflowFolderPickerBtn_Click(object sender, EventArgs e)
        {
            Action settingsPersistence = () => Properties.Settings.Default.currentInflowFolder = inflowFolderPickerDialog.SelectedPath;
            ValidateAndSetFolder(inflowFolderPickerDialog, inflowTxtbox, settingsPersistence);
        }

        private void outputFolderPickerBtn_Click(object sender, EventArgs e)
        {
            Action settingsPersistence = () => Properties.Settings.Default.currentOutputFolder = outputFolderPickerDialog.SelectedPath;
            ValidateAndSetFolder(outputFolderPickerDialog, outputFolderTxtbox, settingsPersistence);
            DuplicateManager.Recache();
        }

        private void duplicatesFolderPickerBtn_Click(object sender, EventArgs e)
        {
            Action settingsPersistence = () => Properties.Settings.Default.currentDuplicateFolder = duplicatesFolderPickerDialog.SelectedPath;
            ValidateAndSetFolder(duplicatesFolderPickerDialog, duplicatesFolderTxtbox, settingsPersistence);
        }

        private void originalsFolderPickerBtn_Click(object sender, EventArgs e)
        {
            Action settingsPersistence = () => Properties.Settings.Default.currentOriginalsFolder = originalsFolderPickerDialog.SelectedPath;
            ValidateAndSetFolder(originalsFolderPickerDialog, originalsFolderTxtbox, settingsPersistence);
            DuplicateManager.Recache();
        }

        private void ValidateAndSetFolder(FolderBrowserDialog dialog, TextBox persistenceTextBox, Action settingsPersistence)
        {
            dialog.SelectedPath = persistenceTextBox.Text;
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }
            String folderPath = dialog.SelectedPath;
            if (string.IsNullOrWhiteSpace(folderPath))
            {
                return;
            }
            if (!Directory.Exists(folderPath))
            {
                return;
            }
            persistenceTextBox.Text = folderPath;
            settingsPersistence.Invoke();
            Properties.Settings.Default.Save();
        }
        #endregion

        #region checkboxes

        private void maleToFemaleTranformCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.shouldTransformMaleToFemale = maleToFemaleTranformCheckbox.Checked;
            Properties.Settings.Default.Save();
        }

        private void intersexToFemaleTransformCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.shouldTransformIntersexToFemale = intersexToFemaleTransformCheckbox.Checked;
            Properties.Settings.Default.Save();
        }

        private void removeDuplicatesCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.shouldRemoveDuplicates = removeDuplicatesCheckbox.Checked;
            Properties.Settings.Default.Save();
            SetDuplicateControlsVisibility();
        }

        private void keepOriginalsCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.shouldKeepOriginalFile = keepOriginalsCheckbox.Checked;
            Properties.Settings.Default.Save();
            SetOutputsControlsVisibility();
        }

        private void SetDuplicateControlsVisibility()
        {
            duplicatesFolderTxtbox.Visible = removeDuplicatesCheckbox.Checked;
            duplicatesFolderPickerBtn.Visible = removeDuplicatesCheckbox.Checked;
        }

        private void SetOutputsControlsVisibility()
        {
            originalsFolderTxtbox.Visible = keepOriginalsCheckbox.Checked;
            originalsFolderPickerBtn.Visible = keepOriginalsCheckbox.Checked;
        }

        #endregion

        #region start/cancel processing
        private void startBtn_Click(object sender, EventArgs e)
        {
            StartProcessing();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            CancelProcessing();
        }

        private void StartProcessing()
        {
            if (fileProcessingWorker.IsBusy)
            {
                return;
            }
            hasStartedProcessingOnce = true;
            SetProcessingControlsVisibility(true);
            elapsedProcessingTimeStopwatch.Restart();
            elapsedProcessingTimer.Start();
            fileProcessingWorker.RunWorkerAsync();
        }

        private void SetProcessingControlsVisibility(bool currentlyEnabled)
        {
            startBtn.Enabled = !currentlyEnabled;
            cancelBtn.Visible = currentlyEnabled;
            processingTimeLbl.Visible = currentlyEnabled || hasStartedProcessingOnce;
            duplicateCountLbl.Visible = (currentlyEnabled || hasStartedProcessingOnce) && removeDuplicatesCheckbox.Checked;
            fileCountLbl.Visible = currentlyEnabled || hasStartedProcessingOnce;
            totalProgressLbl.Visible = currentlyEnabled;
            totalProgress.Visible = currentlyEnabled;
            unprocessableCountLbl.Visible = (currentlyEnabled || hasStartedProcessingOnce) && UnprocessableCountString != "";
            currentFileNameTxtbox.Visible = currentlyEnabled;

            intersexToFemaleTransformCheckbox.Enabled = !currentlyEnabled;
            maleToFemaleTranformCheckbox.Enabled = !currentlyEnabled;
            removeDuplicatesCheckbox.Enabled = !currentlyEnabled;
            keepOriginalsCheckbox.Enabled = !currentlyEnabled;
            inflowFolderPickerBtn.Enabled = !currentlyEnabled;
            outputFolderPickerBtn.Enabled = !currentlyEnabled;
            duplicatesFolderPickerBtn.Enabled = !currentlyEnabled;
            originalsFolderPickerBtn.Enabled = !currentlyEnabled;
        }

        private void CancelProcessing()
        {
            cancelBtn.Enabled = false;
            fileProcessingWorker.CancelAsync();
        }

        private void FinishProcessing()
        {
            SetProcessingControlsVisibility(false);
            elapsedProcessingTimeStopwatch.Stop();
            elapsedProcessingTimer.Stop();
        }

        private void elapsedProcessingTimer_Tick(object sender, EventArgs e)
        {
            if (!elapsedProcessingTimeStopwatch.IsRunning)
            {
                return;
            }
            string elapsedTimeString = elapsedProcessingTimeStopwatch.Elapsed.ToString("hh':'mm':'ss");
            processingTimeLbl.Text = elapsedTimeString;
        }
        #endregion

        #region processing hooks
        private void fileProcessingWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;

            processor.StartProcessing(worker);
        }

        private void fileProcessingWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            fileCountLbl.Text = FileCountString;
            duplicateCountLbl.Text = DuplicateCountString;
            unprocessableCountLbl.Visible = UnprocessableCountString != "";
            unprocessableCountLbl.Text = UnprocessableCountString;
            totalProgress.Value = e.ProgressPercentage;
            if (processor.CurrentFile != null)
            {
                currentFileNameTxtbox.Text = processor.CurrentFile;
            }
        }

        private void fileProcessingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            FinishProcessing();
        }
        #endregion
    }
}