﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextFileStreamliner
{
    static class EncodingDetector
    {
        public static Encoding GetEncoding(Stream stream)
        {
            Ude.CharsetDetector encodingDetector = new Ude.CharsetDetector();
            encodingDetector.Feed(stream);
            encodingDetector.DataEnd();
            string charset = encodingDetector.Charset;
            try
            {
                if (charset.StartsWith("windows-"))
                {
                    int codePage = int.Parse(charset.Replace("windows-", ""));
                    return Encoding.GetEncoding(codePage);
                }
                else
                {
                    return Encoding.GetEncoding(charset);
                }
            }
            catch (Exception)
            {
                Debug.WriteLine($"Tried to use unknown charset {charset}, falling back to UTF-8");
                return Encoding.UTF8;
            }
        }
    }
}
