﻿namespace TextFileStreamliner
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            inflowFolderPickerDialog = new FolderBrowserDialog();
            groupBox1 = new GroupBox();
            currentFileNameTxtbox = new TextBox();
            unprocessableCountLbl = new Label();
            processingTimeLbl = new Label();
            cancelBtn = new Button();
            startBtn = new Button();
            duplicateCountLbl = new Label();
            fileCountLbl = new Label();
            totalProgress = new ProgressBar();
            totalProgressLbl = new Label();
            inflowTxtbox = new TextBox();
            label3 = new Label();
            outputFolderPickerDialog = new FolderBrowserDialog();
            inflowFolderPickerBtn = new Button();
            outputFolderPickerBtn = new Button();
            label4 = new Label();
            outputFolderTxtbox = new TextBox();
            groupBox2 = new GroupBox();
            intersexToFemaleTransformCheckbox = new CheckBox();
            maleToFemaleTranformCheckbox = new CheckBox();
            Tip1 = new ToolTip(components);
            removeDuplicatesCheckbox = new CheckBox();
            keepOriginalsCheckbox = new CheckBox();
            groupBox3 = new GroupBox();
            originalsFolderPickerBtn = new Button();
            originalsFolderTxtbox = new TextBox();
            duplicatesFolderPickerBtn = new Button();
            duplicatesFolderTxtbox = new TextBox();
            duplicatesFolderPickerDialog = new FolderBrowserDialog();
            originalsFolderPickerDialog = new FolderBrowserDialog();
            elapsedProcessingTimer = new System.Windows.Forms.Timer(components);
            fileProcessingWorker = new System.ComponentModel.BackgroundWorker();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            SuspendLayout();
            // 
            // groupBox1
            // 
            groupBox1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            groupBox1.Controls.Add(currentFileNameTxtbox);
            groupBox1.Controls.Add(unprocessableCountLbl);
            groupBox1.Controls.Add(processingTimeLbl);
            groupBox1.Controls.Add(cancelBtn);
            groupBox1.Controls.Add(startBtn);
            groupBox1.Controls.Add(duplicateCountLbl);
            groupBox1.Controls.Add(fileCountLbl);
            groupBox1.Controls.Add(totalProgress);
            groupBox1.Controls.Add(totalProgressLbl);
            groupBox1.Location = new Point(12, 242);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(610, 127);
            groupBox1.TabIndex = 0;
            groupBox1.TabStop = false;
            groupBox1.Text = "Progress";
            // 
            // currentFileNameTxtbox
            // 
            currentFileNameTxtbox.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            currentFileNameTxtbox.Enabled = false;
            currentFileNameTxtbox.Location = new Point(6, 98);
            currentFileNameTxtbox.Name = "currentFileNameTxtbox";
            currentFileNameTxtbox.Size = new Size(598, 23);
            currentFileNameTxtbox.TabIndex = 10;
            currentFileNameTxtbox.Text = "VERY LONG FILE NAME HERE THAT TAKES UP A BUNCH OF SPACE JUST FOR CHECKING STUFF OUT YOU KNOW, IN CASE IT IS SUPER VERY DUPER LONG IN WHICH CASE I'D WANT TO SEE SOMETHING AT LEAST, RIGHT?";
            // 
            // unprocessableCountLbl
            // 
            unprocessableCountLbl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            unprocessableCountLbl.AutoSize = true;
            unprocessableCountLbl.Location = new Point(247, 57);
            unprocessableCountLbl.Name = "unprocessableCountLbl";
            unprocessableCountLbl.Size = new Size(114, 15);
            unprocessableCountLbl.TabIndex = 9;
            unprocessableCountLbl.Text = "Unprocessable: 9999";
            // 
            // processingTimeLbl
            // 
            processingTimeLbl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            processingTimeLbl.AutoSize = true;
            processingTimeLbl.BackColor = Color.Transparent;
            processingTimeLbl.Location = new Point(7, 57);
            processingTimeLbl.Name = "processingTimeLbl";
            processingTimeLbl.Size = new Size(49, 15);
            processingTimeLbl.TabIndex = 8;
            processingTimeLbl.Text = "00:00:00";
            // 
            // cancelBtn
            // 
            cancelBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            cancelBtn.Location = new Point(88, 24);
            cancelBtn.Name = "cancelBtn";
            cancelBtn.Size = new Size(75, 23);
            cancelBtn.TabIndex = 7;
            cancelBtn.Text = "Cancel";
            cancelBtn.UseVisualStyleBackColor = true;
            cancelBtn.Click += cancelBtn_Click;
            // 
            // startBtn
            // 
            startBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            startBtn.Location = new Point(7, 24);
            startBtn.Name = "startBtn";
            startBtn.Size = new Size(75, 23);
            startBtn.TabIndex = 6;
            startBtn.Text = "Start";
            startBtn.UseVisualStyleBackColor = true;
            startBtn.Click += startBtn_Click;
            // 
            // duplicateCountLbl
            // 
            duplicateCountLbl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            duplicateCountLbl.AutoSize = true;
            duplicateCountLbl.Location = new Point(149, 57);
            duplicateCountLbl.Name = "duplicateCountLbl";
            duplicateCountLbl.Size = new Size(92, 15);
            duplicateCountLbl.TabIndex = 5;
            duplicateCountLbl.Text = "Duplicates: 9999";
            // 
            // fileCountLbl
            // 
            fileCountLbl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            fileCountLbl.AutoSize = true;
            fileCountLbl.Location = new Point(62, 57);
            fileCountLbl.Name = "fileCountLbl";
            fileCountLbl.Size = new Size(81, 15);
            fileCountLbl.TabIndex = 4;
            fileCountLbl.Text = "File 9999/9999";
            // 
            // totalProgress
            // 
            totalProgress.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            totalProgress.Location = new Point(76, 78);
            totalProgress.Name = "totalProgress";
            totalProgress.Size = new Size(529, 15);
            totalProgress.TabIndex = 2;
            // 
            // totalProgressLbl
            // 
            totalProgressLbl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            totalProgressLbl.AutoSize = true;
            totalProgressLbl.Location = new Point(7, 78);
            totalProgressLbl.Name = "totalProgressLbl";
            totalProgressLbl.Size = new Size(32, 15);
            totalProgressLbl.TabIndex = 0;
            totalProgressLbl.Text = "Total";
            // 
            // inflowTxtbox
            // 
            inflowTxtbox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            inflowTxtbox.Enabled = false;
            inflowTxtbox.Location = new Point(105, 12);
            inflowTxtbox.Name = "inflowTxtbox";
            inflowTxtbox.PlaceholderText = "Select a folder with the picker on the right";
            inflowTxtbox.ReadOnly = true;
            inflowTxtbox.Size = new Size(484, 23);
            inflowTxtbox.TabIndex = 1;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(12, 15);
            label3.Name = "label3";
            label3.Size = new Size(76, 15);
            label3.TabIndex = 2;
            label3.Text = "Inflow Folder";
            // 
            // inflowFolderPickerBtn
            // 
            inflowFolderPickerBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            inflowFolderPickerBtn.Location = new Point(595, 12);
            inflowFolderPickerBtn.Name = "inflowFolderPickerBtn";
            inflowFolderPickerBtn.Size = new Size(27, 23);
            inflowFolderPickerBtn.TabIndex = 3;
            inflowFolderPickerBtn.Text = "...";
            inflowFolderPickerBtn.UseVisualStyleBackColor = true;
            inflowFolderPickerBtn.Click += inflowFolderPickerBtn_Click;
            // 
            // outputFolderPickerBtn
            // 
            outputFolderPickerBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            outputFolderPickerBtn.Location = new Point(595, 41);
            outputFolderPickerBtn.Name = "outputFolderPickerBtn";
            outputFolderPickerBtn.Size = new Size(27, 23);
            outputFolderPickerBtn.TabIndex = 6;
            outputFolderPickerBtn.Text = "...";
            outputFolderPickerBtn.UseVisualStyleBackColor = true;
            outputFolderPickerBtn.Click += outputFolderPickerBtn_Click;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(12, 44);
            label4.Name = "label4";
            label4.Size = new Size(81, 15);
            label4.TabIndex = 5;
            label4.Text = "Output Folder";
            // 
            // outputFolderTxtbox
            // 
            outputFolderTxtbox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            outputFolderTxtbox.Enabled = false;
            outputFolderTxtbox.Location = new Point(105, 41);
            outputFolderTxtbox.Name = "outputFolderTxtbox";
            outputFolderTxtbox.PlaceholderText = "Select a folder with the picker on the right";
            outputFolderTxtbox.ReadOnly = true;
            outputFolderTxtbox.Size = new Size(484, 23);
            outputFolderTxtbox.TabIndex = 4;
            // 
            // groupBox2
            // 
            groupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            groupBox2.Controls.Add(intersexToFemaleTransformCheckbox);
            groupBox2.Controls.Add(maleToFemaleTranformCheckbox);
            groupBox2.Location = new Point(12, 70);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(610, 77);
            groupBox2.TabIndex = 7;
            groupBox2.TabStop = false;
            groupBox2.Text = "Content-Modifiers";
            // 
            // intersexToFemaleTransformCheckbox
            // 
            intersexToFemaleTransformCheckbox.AutoSize = true;
            intersexToFemaleTransformCheckbox.Location = new Point(6, 47);
            intersexToFemaleTransformCheckbox.Name = "intersexToFemaleTransformCheckbox";
            intersexToFemaleTransformCheckbox.Size = new Size(124, 19);
            intersexToFemaleTransformCheckbox.TabIndex = 1;
            intersexToFemaleTransformCheckbox.Text = "Intersex -> Female";
            Tip1.SetToolTip(intersexToFemaleTransformCheckbox, "Replaces mentions of intersex pronouns with those of female ones. Stuff like \"hir\", \"hyr\", etc.");
            intersexToFemaleTransformCheckbox.UseVisualStyleBackColor = true;
            intersexToFemaleTransformCheckbox.CheckedChanged += intersexToFemaleTransformCheckbox_CheckedChanged;
            // 
            // maleToFemaleTranformCheckbox
            // 
            maleToFemaleTranformCheckbox.AutoSize = true;
            maleToFemaleTranformCheckbox.Location = new Point(6, 22);
            maleToFemaleTranformCheckbox.Name = "maleToFemaleTranformCheckbox";
            maleToFemaleTranformCheckbox.Size = new Size(109, 19);
            maleToFemaleTranformCheckbox.TabIndex = 0;
            maleToFemaleTranformCheckbox.Text = "Male -> Female";
            Tip1.SetToolTip(maleToFemaleTranformCheckbox, "Replaces mentions of male pronouns with those of female ones. Includes common words like \"dude\",etc.");
            maleToFemaleTranformCheckbox.UseVisualStyleBackColor = true;
            maleToFemaleTranformCheckbox.CheckedChanged += maleToFemaleTranformCheckbox_CheckedChanged;
            // 
            // removeDuplicatesCheckbox
            // 
            removeDuplicatesCheckbox.AutoSize = true;
            removeDuplicatesCheckbox.Location = new Point(6, 22);
            removeDuplicatesCheckbox.Name = "removeDuplicatesCheckbox";
            removeDuplicatesCheckbox.Size = new Size(127, 19);
            removeDuplicatesCheckbox.TabIndex = 0;
            removeDuplicatesCheckbox.Text = "Remove Duplicates";
            Tip1.SetToolTip(removeDuplicatesCheckbox, "Checks if the output folder contains the file already and moves it to the duplicates folder instead of processing");
            removeDuplicatesCheckbox.UseVisualStyleBackColor = true;
            removeDuplicatesCheckbox.CheckedChanged += removeDuplicatesCheckbox_CheckedChanged;
            // 
            // keepOriginalsCheckbox
            // 
            keepOriginalsCheckbox.AutoSize = true;
            keepOriginalsCheckbox.Location = new Point(6, 51);
            keepOriginalsCheckbox.Name = "keepOriginalsCheckbox";
            keepOriginalsCheckbox.Size = new Size(100, 19);
            keepOriginalsCheckbox.TabIndex = 9;
            keepOriginalsCheckbox.Text = "Keep originals";
            Tip1.SetToolTip(keepOriginalsCheckbox, "After transforming files, move the original file to this folder");
            keepOriginalsCheckbox.UseVisualStyleBackColor = true;
            keepOriginalsCheckbox.CheckedChanged += keepOriginalsCheckbox_CheckedChanged;
            // 
            // groupBox3
            // 
            groupBox3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            groupBox3.Controls.Add(originalsFolderPickerBtn);
            groupBox3.Controls.Add(originalsFolderTxtbox);
            groupBox3.Controls.Add(keepOriginalsCheckbox);
            groupBox3.Controls.Add(duplicatesFolderPickerBtn);
            groupBox3.Controls.Add(duplicatesFolderTxtbox);
            groupBox3.Controls.Add(removeDuplicatesCheckbox);
            groupBox3.Location = new Point(12, 153);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(610, 83);
            groupBox3.TabIndex = 8;
            groupBox3.TabStop = false;
            groupBox3.Text = "File-Modifiers";
            // 
            // originalsFolderPickerBtn
            // 
            originalsFolderPickerBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            originalsFolderPickerBtn.Location = new Point(572, 49);
            originalsFolderPickerBtn.Name = "originalsFolderPickerBtn";
            originalsFolderPickerBtn.Size = new Size(27, 23);
            originalsFolderPickerBtn.TabIndex = 11;
            originalsFolderPickerBtn.Text = "...";
            originalsFolderPickerBtn.UseVisualStyleBackColor = true;
            originalsFolderPickerBtn.Click += originalsFolderPickerBtn_Click;
            // 
            // originalsFolderTxtbox
            // 
            originalsFolderTxtbox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            originalsFolderTxtbox.Enabled = false;
            originalsFolderTxtbox.Location = new Point(139, 49);
            originalsFolderTxtbox.Name = "originalsFolderTxtbox";
            originalsFolderTxtbox.PlaceholderText = "Select a folder with the picker on the right";
            originalsFolderTxtbox.ReadOnly = true;
            originalsFolderTxtbox.Size = new Size(427, 23);
            originalsFolderTxtbox.TabIndex = 10;
            // 
            // duplicatesFolderPickerBtn
            // 
            duplicatesFolderPickerBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            duplicatesFolderPickerBtn.Location = new Point(572, 20);
            duplicatesFolderPickerBtn.Name = "duplicatesFolderPickerBtn";
            duplicatesFolderPickerBtn.Size = new Size(27, 23);
            duplicatesFolderPickerBtn.TabIndex = 8;
            duplicatesFolderPickerBtn.Text = "...";
            duplicatesFolderPickerBtn.UseVisualStyleBackColor = true;
            duplicatesFolderPickerBtn.Click += duplicatesFolderPickerBtn_Click;
            // 
            // duplicatesFolderTxtbox
            // 
            duplicatesFolderTxtbox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            duplicatesFolderTxtbox.Enabled = false;
            duplicatesFolderTxtbox.Location = new Point(139, 20);
            duplicatesFolderTxtbox.Name = "duplicatesFolderTxtbox";
            duplicatesFolderTxtbox.PlaceholderText = "Select a folder with the picker on the right";
            duplicatesFolderTxtbox.ReadOnly = true;
            duplicatesFolderTxtbox.Size = new Size(427, 23);
            duplicatesFolderTxtbox.TabIndex = 7;
            // 
            // elapsedProcessingTimer
            // 
            elapsedProcessingTimer.Tick += elapsedProcessingTimer_Tick;
            // 
            // fileProcessingWorker
            // 
            fileProcessingWorker.WorkerReportsProgress = true;
            fileProcessingWorker.WorkerSupportsCancellation = true;
            fileProcessingWorker.DoWork += fileProcessingWorker_DoWork;
            fileProcessingWorker.ProgressChanged += fileProcessingWorker_ProgressChanged;
            fileProcessingWorker.RunWorkerCompleted += fileProcessingWorker_RunWorkerCompleted;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(634, 381);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Controls.Add(outputFolderPickerBtn);
            Controls.Add(label4);
            Controls.Add(outputFolderTxtbox);
            Controls.Add(inflowFolderPickerBtn);
            Controls.Add(label3);
            Controls.Add(inflowTxtbox);
            Controls.Add(groupBox1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MinimumSize = new Size(650, 420);
            Name = "MainForm";
            Text = "Text File Streamliner";
            Load += MainWindow_Load;
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private FolderBrowserDialog inflowFolderPickerDialog;
        private GroupBox groupBox1;
        private Label totalProgressLbl;
        private ProgressBar totalProgress;
        private Label fileCountLbl;
        private TextBox inflowTxtbox;
        private Label label3;
        private FolderBrowserDialog outputFolderPickerDialog;
        private Button inflowFolderPickerBtn;
        private Button outputFolderPickerBtn;
        private Label label4;
        private TextBox outputFolderTxtbox;
        private GroupBox groupBox2;
        private CheckBox intersexToFemaleTransformCheckbox;
        private CheckBox maleToFemaleTranformCheckbox;
        private ToolTip Tip1;
        private GroupBox groupBox3;
        private Button duplicatesFolderPickerBtn;
        private TextBox duplicatesFolderTxtbox;
        private CheckBox removeDuplicatesCheckbox;
        private FolderBrowserDialog duplicatesFolderPickerDialog;
        private Button originalsFolderPickerBtn;
        private TextBox originalsFolderTxtbox;
        private CheckBox keepOriginalsCheckbox;
        private FolderBrowserDialog originalsFolderPickerDialog;
        private Button cancelBtn;
        private Button startBtn;
        private Label duplicateCountLbl;
        private Label processingTimeLbl;
        private System.Windows.Forms.Timer elapsedProcessingTimer;
        private System.ComponentModel.BackgroundWorker fileProcessingWorker;
        private Label unprocessableCountLbl;
        private TextBox currentFileNameTxtbox;
    }
}