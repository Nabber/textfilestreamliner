# Text file streamliner
A personal tool to extract content from pdf, rtf, txt and word (doc, docx, odt) files.  
The content can have basic replacements applied to them and are then saved as .rtf files with font Arial and font-size 14 (personal preference).

TODO:
* Multi-thread the file processing
* Use different framework to extract word file content (current one is extremely slow)
* Make font properties modifiable
* In-UI customizable regex replacements